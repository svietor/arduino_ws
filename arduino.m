classdef arduino
    %Arduino Class designed to interface with an Arduino over Serial
    %Connection
    
    properties
        %Connection Properties
        BaudRate = 115200
        COM = 'COM4'
        SerialPort
        Status = 'closed'
        
        %Communication Interface
        sendTemplate = "%c:%c:%.10u"
        receiveTemplate = "%c:%c:32u"
        
        startMarker = '<';
        endMarker   = '>';
        
    end
    
    methods
        function obj = arduino(COM,BaudRate)
            %arduino Construct an instance of this class
            %   Detailed explanation goes here
            obj.BaudRate   = BaudRate;
            obj.COM        = COM;
            obj.SerialPort = serial(obj.COM,'Baudrate',obj.BaudRate);
            
        end
        
        function obj = open(obj)
            
            % Open SerialPort
            fopen(obj.SerialPort);
            status = obj.SerialPort.status;
            obj.Status = status;
            
            pause(2) %Wait for reset
            
        end
        
        function obj = close(obj)
            fclose(obj.SerialPort);
            obj.Status = obj.SerialPort.status;
        end
        
        function executeStep(obj,dir,nm)
            
            %Message
            if dir == 1
                pkt_out = obj.makePacket('Execute','FW Steps',nm);
            elseif dir == 0
                pkt_out = obj.makePacket('Execute','REV Steps',nm);
            end
            
            %Sent set message to arduino
            obj.transmit(pkt_out);
        end
        
        function val = getMotorPosition(obj)
            
            %Message
            pkt_out = obj.makePacket('Get','Motor Position');
            
            %Sent set message to arduino
            obj.transmit(pkt_out);
            
            %Receive response from arduino
            pkt_in = obj.receive();
            
            %Parse response
            val_nm = obj.parsePacket(pkt_in);
            
            val = val_nm*10e-9;
            
        end
        
        function val = getEncoderPosition(obj)
            
            %Message
            pkt_out = obj.makePacket('Get','Encoder Position');
            
            %Sent set message to arduino
            obj.transmit(pkt_out);
            
            %Receive response from arduino
            pkt_in = obj.receive();
            
            %Parse response
            val_nm = obj.parsePacket(pkt_in);
            val = val_nm * 10e-9;
            
        end
        
        function val = getShutterAngle(obj)
            
            %Message
            pkt_out = obj.makePacket('Get','Shutter Angle');
            
            %Sent set message to arduino
            obj.transmit(pkt_out);
            
            %Receive response from arduino
            pkt_in = obj.receive();
            
            %Parse response
            val = obj.parsePacket(pkt_in);
        end
        
        function executeMotion(obj,val)
            
            %Convert to nm
            val_nm = val*10e-9;
            
            %Message
            pkt_out = obj.makePacket('Execute','Motion',val_nm);
            
            %Sent set message to arduino
            obj.transmit(pkt_out);
            
        end
        
        function executeAbort(obj)
            
            %Message
            pkt_out = obj.makePacket('Execute','Abort');
            
            %Sent set message to arduino
            obj.transmit(pkt_out);
            
        end
        
        function executeCalibration(obj)
            
            %Message
            pkt_out = obj.makePacket('Execute','Calibration',0);
            
            %Sent set message to arduino
            obj.transmit(pkt_out);
        end
        
        function setShutterAngle(obj,val)
            
            %Message
            pkt_out = obj.makePacket('Set','Shutter Angle',val);
            
            %Sent set message to arduino
            obj.transmit(pkt_out);
            
        end
        
    end
    
    methods (Access = private)
        
        function pkt = makePacket(obj,action,property,val)
            
            %action: Char
            %property: Char
            %val: Unsigned Int
            
            switch (property)
                case 'Motor Position'
                    P = 'P';
                case 'Encoder Position'
                    P = 'N';
                case 'Shutter Angle'
                    P = 'S';
                case 'Motion'
                    P = 'M';
                case 'Calibration'
                    P = 'C';
                case 'FW Steps'
                    P = 'F';
                case 'REV Steps'
                    P = 'R';
                case 'Abort'
                    P = 'A';
            end
            
            switch(action)
                case 'Execute'
                    A = 'E';
                    val = floor(val);
                case 'Set'
                    A = 'S';
                    val = floor(val);
                case 'Get'
                    A = 'G';
                    val = 0;
            end
            
            %Message format: '<[action]:[property]:[variable]>'
            pkt = sprintf(obj.sendTemplate,A,P,val);
            
        end
        
        function value = parsePacket(obj,pkt)
            %Message format: '<[action]:[property]:[value]>'
            message  = textscan(pkt,obj.receiveTemplate);
            value = message(3);
        end
        
        function obj = transmit(obj,pkt)
            fprintf(obj.SerialPort,pkt);
        end
        
        function pkt = receive(obj)
            
            pkt = "";
            ch = "";
            
            while ~obj.SerialPort.BytesAvailable() %NOT NECESSARY?
                pause(0.001);
            end
            
            while  ~strcmp(ch,obj.startMarker)
                ch = char(fread(obj.SerialPort,1,'char'));
            end
            
            while ~strcmp(ch,obj.endMarker)
                if ~strcmp(ch,obj.startMarker)
                    pkt = pkt + ch;
                end
                ch = char(fread(obj.SerialPort,1,'char'));
            end
        end
        
    end
    
    
end



