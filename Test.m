%% Arduino Communication Interface %%
% Test Script

%% Init
clear all
close all
clc

%close all COM ports

if ~isempty(instrfind)
    fclose(instrfind);
end

%list COM ports
%seriallist

%% Constants

BaudRate = 115200;
COM = 'COM4';

%% Initialize Object
a = arduino(COM,BaudRate);
a.open();

%% Step

nm = 50000000;

a.makePacket('Execute','FW Steps',nm)
a.Step(1,nm);
a.Step(0,nm);

%% Calibration

a.executeCalibration()

%% Close

