%% COM Test %%

%% Init

clear all
close all
clc

%close all COM ports

if ~isempty(instrfind)
    fclose(instrfind);
end

%list COM ports
seriallist

%% Generate COM port
BaudRate = 9600;
COM = 'COM4';
s = serial(COM,'BaudRate',BaudRate);


try 
    fopen(s)
catch
    disp("COM already open")
end


%% Test

command = "<G:P:0>";
fprintf(s,command)

pkt = "";
ch = '';

while ~s.BytesAvailable()
    pause(0.001);
end

while  ~strcmp(ch,'<')
    ch = char(fread(s,1,'char'));
end

while ~strcmp(ch,'>') 
    if ~strcmp(ch,'<')
        pkt = pkt + ch;
    end
    ch = char(fread(s,1,'char'));
end




