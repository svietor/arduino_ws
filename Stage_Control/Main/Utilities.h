#ifndef UTILITIES_H
#define UTILITIES_H

unsigned long stp2nm(unsigned long step_val, unsigned long nm_pitch);

unsigned long nm2stp(unsigned long nm_val,unsigned long nm_pitch);

unsigned long tck2nm(unsigned long tick_val, unsigned long nm_res);

unsigned long nm2tck(unsigned long nm_val,unsigned long nm_res);

#endif
