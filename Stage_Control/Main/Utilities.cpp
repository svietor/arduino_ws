#include "Utilities.h"

//******************Functions*****************//

unsigned long stp2nm(unsigned long step_val, unsigned long nm_pitch){
  return step_val*nm_pitch;
}

unsigned long nm2stp(unsigned long nm_val,unsigned long nm_pitch){
  return nm_val/nm_pitch;
}

unsigned long tck2nm(unsigned long tick_val, unsigned long nm_res){
    return tick_val*nm_res;

}

unsigned long nm2tck(unsigned long nm_val,unsigned long nm_res){
  return nm_val/nm_res;

}

//********************************************//
