#include "Hardware.h"

extern bool DEBUG;

extern AutoDriver Stage;
extern LS7366R Encoder;
extern unsigned long motorPosition;
extern bool executeCalibration;
extern bool isCalibrated;

void setupAutoDriver() {
  
  // Start by setting up the SPI port and pins.
  pinMode(RESET_PIN, OUTPUT);
  pinMode(MOSI, OUTPUT);
  pinMode(MISO, INPUT);
  pinMode(13, OUTPUT); ////WAS IST DAS??
  pinMode(AUTODRIVER_CS, OUTPUT);
  digitalWrite(AUTODRIVER_CS, HIGH);
  digitalWrite(RESET_PIN, LOW);
  digitalWrite(RESET_PIN, HIGH);
  SPI.begin();
  //SPI.setDataMode(SPI_MODE3);

  // Before we do anything, we need to tell each board which SPI
  //  port we're using. Most of the time, there's only the one,
  //  but it's possible for some larger Arduino boards to have more
  //  than one, so don't take it for granted.
  Stage.SPIPortConnect(&SPI);

  // Set the Overcurrent Threshold to 6A.
  Stage.setOCThreshold(OCD_TH_6000mA);

  //Hard stop when endswitch is hit
  Stage.setSwitchMode(SW_HARD_STOP);

  // KVAL is a modifier that sets the effective voltage applied
  //  to the motor. KVAL/255 * Vsupply = effective motor voltage.
  Stage.setRunKVAL(RUN_KVAL);  // 128/255 * 12V = 6V
  Stage.setAccKVAL(ACC_KVAL);  // 192/255 * 12V = 9V
  Stage.setDecKVAL(DEC_KVAL);
  Stage.setHoldKVAL(HOLD_KVAL);  // 32/255 * 12V = 1.5V

  // enable Microstepping to reduce cogging torque vibrations
  //Stage.configStepMode(STEP_FS_128);
  Stage.configStepMode(STEP_FS_32);

  // When a move command is issued, max speed is the speed the
  //  motor tops out at while completing the move, in steps/s
  Stage.setMaxSpeed(SPEED_VAL);

  // Enable low-Speed Optimization
  Stage.setLoSpdOpt(true);

  // Acceleration and deceleration in steps/s/s. Increasing this
  //  value makes the motor reach its full speed more quickly,
  //  at the cost of possibly causing it to slip and miss steps.
  Stage.setAcc(ACCEL_VAL);
  Stage.setDec(DECEL_VAL);

  }

bool statusAutoDriver() {

  int paramValue;
  bool status = true;

  paramValue = Stage.getParam(CONFIG);

  if (paramValue != STATUS_OK_BYTE)
  {
    status = false;
  }

  //Serial.println("CONFIG register: ");
  //Serial.println(paramValue,HEX);
  return status;
}

void stageCalibration(){

  unsigned long encoderEndstopVal;
  unsigned long encoderVal;
  unsigned long motorVal;

  delayMicroseconds(SPI_DELAY);
  encoderVal = Encoder.readEncoder();
  
  delayMicroseconds(SPI_DELAY);
  Stage.goUntil(RESET_ABSPOS,REV,float(SPEED_VAL));

  while(Stage.busyCheck()){  //wait untill endstop is reached
     delay(100);
    if (DEBUG){
      Serial.println("Moving to endstop...");
    }
  }

  // Reconfigure Encoder buffer to reset at index pulses
  delayMicroseconds(SPI_DELAY);
  Encoder.configMDR(MDR0_RST, MDR1);
  
  // Read encoder count at endstop
  delayMicroseconds(SPI_DELAY);
  encoderEndstopVal =  Encoder.readEncoder();
  
  //Go to the nearest Encoder index pulse

  encoderVal = encoderEndstopVal;

  if (DEBUG){
    Serial.println("Moving to nearest encoder index tick");
    Serial.println("");
  }
  
  while (encoderVal > CALIBRATION_MARGIN){
    
    delay(100);
    Stage.move(FWD,CALIBRATION_STEP_SIZE);

    while(Stage.busyCheck()){  //wait untill motion is executed
    delay(10);
    }
    
    delayMicroseconds(SPI_DELAY);
    encoderVal = Encoder.readEncoder();

    if (DEBUG){
      Serial.print("Encoder Count: ");
      Serial.println(encoderVal);
    }
    
  }
  
  delayMicroseconds(SPI_DELAY);
  Stage.setPos(nm2stp(tck2nm(encoderVal,RES_NM),PITCH_NM)); //Synchronise motor position with encoder position

  
  delayMicroseconds(SPI_DELAY);
  motorPosition = Stage.getPos();

  if(DEBUG){
    Serial.print("Encoder [nm] After Calibration Routine: ");
    Serial.println(tck2nm(encoderVal,RES_NM));
    Serial.print("Motor [nm] after Calibration Routine: ");
    Serial.println(stp2nm(motorPosition,PITCH_NM));
  }

   // Reconfigure Encoder buffer to initial settings
  delayMicroseconds(SPI_DELAY);
  Encoder.configMDR(MDR0, MDR1);

  
  
}
