#include "Interface.h"
#include <Arduino.h>

extern bool DEBUG;

extern bool pktError;
extern bool pktReceived;

extern struct Message incomingMessage;
extern struct Message outgoingMessage;

char inputBuffer[BUFFER_SIZE];
char outputBuffer[BUFFER_SIZE + 2]; //CHANGE SIZE
char processBuffer[BUFFER_SIZE]; //Same size as inputBuffer


void receivePacket() {
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char rc;

  while (Serial.available() > 0 && pktReceived == false) {
    rc = Serial.read();

    if (recvInProgress == true) {
      if (rc != END_MARKER) {
        inputBuffer[ndx] = rc;
        ndx++;
        if (ndx >= BUFFER_SIZE) {
          ndx = BUFFER_SIZE - 1;

        }
      }
      else {
        inputBuffer[ndx] = '\0'; // terminate the string
        recvInProgress = false;
        ndx = 0;
        pktReceived = true;
      }
    }

    else if (rc == START_MARKER) {
      recvInProgress = true;
    }
  }
}

void sendPacket(){
  //MessageTemplate: <[action]:[property]:[value]>
  // value is int
  
  if (pktError == false){
    sprintf(outputBuffer,"<%c:%c:%.10lu>",outgoingMessage.action[0],outgoingMessage.property[0],outgoingMessage.value);
    Serial.print(outputBuffer);
  }
  else{

    if (DEBUG){
      Serial.println();
      Serial.print("Message Error");
      pktError = false;
    }
  }
}

void parsePkt(){

  //Message Template: <[action]:[property]:[value]>

  char * strtokIndx; // this is used by strtok() as an index

  strcpy(processBuffer, inputBuffer);
    // this temporary copy is necessary to protect the original data
    // because strtok() used in parseData() replaces the commas with \0

  strtokIndx = strtok(processBuffer, DELIM);     // get the first character (action)

  strcpy(incomingMessage.action, strtokIndx);

  //Serial.print("Message Parsing...[Action] = ");
  //Serial.print(incomingMessage.action);

  strtokIndx = strtok(NULL, DELIM);     // get the second character (property)
  strcpy(incomingMessage.property, strtokIndx); //

  //Serial.print("Message Parsing...[Property] = ");
  //Serial.print(incomingMessage.property);

  strtokIndx = strtok(NULL, DELIM);
  //incomingMessage.value = atoi(strtokIndx);     // get the rest (value) and convert it to a long
  incomingMessage.value = strtoul(strtokIndx, NULL, 10);

  //Serial.print("Message Parsing...[value] = ");
  //Serial.print(incomingMessage.value);

}
