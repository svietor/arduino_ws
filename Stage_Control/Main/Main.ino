#include "Hardware.h"
#include "Interface.h"
#include "Utilities.h"



//****************Variables/Objects***********//

//Debug

bool DEBUG = false; //Enable debug messages over the COM port

// Hardware
AutoDriver      Stage(0,AUTODRIVER_CS, RESET_PIN);
LS7366R         Encoder(ENCODER_CS, MDR0, MDR1);
Servo           Shutter;


// Communication Interface
struct Message incomingMessage;
struct Message outgoingMessage;


// System State
unsigned long   motorPosition     = 0L;   // Motor position [mu steps]
unsigned long   encoderPosition   = 0L;   // Encoder position [ticks]
unsigned long   shutterAngle      = 0;    // shutter position [deg]
unsigned long   shutterAngleSet;          // shutter position setpoint [deg]
unsigned long   motorPositionSet;         // Position setpoint [mu steps]
unsigned long   stepsToTake      = 0L;    // direction for stepping functions
int             stateRelay1      = 0;      // relay state (1->on, 0->off)
int             stateRelay2      = 0;
int             stateRelay3      = 0;
int             stateRelay4      = 0;

bool            isBusy           = false; //
bool            isCalibrated     = false;



// Action Flags
bool  pktReceived         = false;
bool  pktError            = false;
bool  updateMotion        = false;
bool  updateShutter       = false;
bool  executeCalibration  = false;
bool  executeAbort        = false;
bool  executeFwdStep      = false;
bool  executeRevStep      = false;
bool  updateRelay1        = false;
bool  updateRelay2        = false;
bool  updateRelay3        = false;
bool  updateRelay4        = false;

//********************************************//


//***************Setup************************//
void setup() {
  
  //Initialize Serial Communication
  Serial.begin(BAUD_RATE);

  //Initilize AutoDriver
  setupAutoDriver();
  //Status.driver = statusAutoDriver();

  //Initialize Encoder
  Encoder.clearEncoder();

  //Initialize Servo
  Shutter.attach(SHUTTER_SERVO_PIN);

  //Initialize Relays
  pinMode(RELAY_1_PIN, OUTPUT);
  pinMode(RELAY_2_PIN, OUTPUT);
  pinMode(RELAY_3_PIN, OUTPUT);
  pinMode(RELAY_4_PIN, OUTPUT);

  if (DEBUG){
    Serial.println("System Setup Complete");
  }
   
}

void loop() {

  //Check for Serial Messages
  receivePacket();

  //Process Serial Message
  if (pktReceived) {

    updateState();  // Update State Variables
    parsePkt();     // Parse Serial Message
    processPkt();   // Process Message and set action flags accordingly
    sendPacket();   // Reply packet
    pktReceived = false; 
  }
  
  //Update the system based on action flags
  updateSystem();

}


void updateSystem() {

  if (executeAbort){
   Stage.softStop();
   executeAbort = false;
  }

  // Stepper motor interaction
  if (!isBusy){

    if (executeCalibration){

      stageCalibration();
      executeCalibration = false;
      isCalibrated = true;      
    }

    if (updateMotion && isCalibrated){ //Go to position setpoint only works if stage is calibrated
      Stage.goTo(motorPositionSet);
      updateMotion = false;
    }

    if (executeFwdStep || executeRevStep){

      if(executeFwdStep){
        Stage.move(FWD,stepsToTake);
        executeFwdStep = false;
      }

      if(executeRevStep){
        Stage.move(REV,stepsToTake);
        executeRevStep = false;
      }
    }
  }

  if (updateShutter)
  {
    Shutter.write(shutterAngleSet);
  }
}

void updateState(){

  delayMicroseconds(SPI_DELAY);
  isBusy = Stage.busyCheck();

  if (isCalibrated) //Positions are only meaningfull after stage calibration
  {
    //Motor Position
    delayMicroseconds(SPI_DELAY);
    motorPosition = Stage.getPos();
   

    //Encoder Position
     delayMicroseconds(SPI_DELAY);
    encoderPosition = Encoder.readEncoder();
  }

  shutterAngle = Shutter.read();

}

void processPkt(){

  switch (incomingMessage.action[0]) {

    case CHECK_CHAR:
    
    switch (incomingMessage.property[0]) {
      case CALIBRATION_CHAR:
      outgoingMessage.property[0] = CALIBRATION_CHAR;
      outgoingMessage.value = isCalibrated;

      if (DEBUG){
        Serial.println();
        Serial.print("Check Calibration: ");
        Serial.println(isCalibrated);
      }
      
      break;

      case IS_BUSY_CHAR:
      outgoingMessage.property[0] = IS_BUSY_CHAR;
      outgoingMessage.value = isCalibrated;

      if (DEBUG){
        Serial.println();
        Serial.print("Check Calibration: ");
        Serial.println(isCalibrated);
      }
        
      default:

      if (DEBUG){
        Serial.println();
        Serial.println("Command Not Recognized");
        pktError = true;
      }
      
      break;
    }

    case GET_CHAR: //Get
    switch (incomingMessage.property[0]) {

      case SHUTTER_POSITION_CHAR:
      outgoingMessage.property[0] = SHUTTER_POSITION_CHAR;
      outgoingMessage.value       = shutterAngle;

      if (DEBUG){
        Serial.println();
        Serial.print("Get Shutter Angle [deg]: ");
        Serial.println(shutterAngle);
      }
      
      break;

      case MOTOR_POSITION_CHAR:
      outgoingMessage.property[0] = MOTOR_POSITION_CHAR;
      outgoingMessage.value       = stp2nm(motorPosition,PITCH_NM);

      if (DEBUG){
        Serial.println();
        Serial.print("Get Position Motor [nm]: ");
        Serial.println(stp2nm(motorPosition,PITCH_NM));
      }
      
      break;

      case ENCODER_POSITION_CHAR:
      outgoingMessage.property[0] = ENCODER_POSITION_CHAR;
      outgoingMessage.value       = tck2nm(encoderPosition,RES_NM);

      if (DEBUG){
        Serial.println();
        Serial.print("Get Position Encoder [nm]: ");
        Serial.println(tck2nm(encoderPosition,RES_NM));
      }
      
      break;

      default:

      if (DEBUG){
        Serial.println();
        Serial.println("Command Not Recognized");
        pktError = true;
      }
      
      break;
    }
    outgoingMessage.action[0] = GET_CHAR;
    break;

    case SET_CHAR: //Set
    switch (incomingMessage.property[0]) {

      case SHUTTER_POSITION_CHAR:
      shutterAngleSet = incomingMessage.value;
      
      outgoingMessage.property[0] = SHUTTER_POSITION_CHAR;
      outgoingMessage.value       = shutterAngleSet;

      updateShutter = true;

      if (DEBUG){
        Serial.println();
        Serial.print("Set Shutter Angle [deg]: ");
        Serial.println(shutterAngleSet);
      }
      break;

      case RELAY_1_CHAR:
      stateRelay1 = incomingMessage.value;
      
      outgoingMessage.property[0] = RELAY_1_CHAR;
      outgoingMessage.value       = stateRelay1;

      updateRelay1 = true;

      if (DEBUG){
        Serial.println();
        Serial.print("Set Relay 1 to: ");
        Serial.println(stateRelay1);
      }
      break;

      case RELAY_2_CHAR:
      stateRelay1 = incomingMessage.value;
      
      outgoingMessage.property[0] = RELAY_2_CHAR;
      outgoingMessage.value       = stateRelay2;

      updateRelay2 = true;

      if (DEBUG) {
        Serial.println();
        Serial.print("Set Relay 2 to: ");
        Serial.println(stateRelay2);
      }
      break;

      case RELAY_3_CHAR:
      stateRelay3 = incomingMessage.value;
      
      outgoingMessage.property[0] = RELAY_3_CHAR;
      outgoingMessage.value       = stateRelay3;

      updateRelay3 = true;

      if (DEBUG){
        Serial.println();
        Serial.print("Set Relay 3 to: ");
        Serial.println(stateRelay3);
      }
      break;

      case RELAY_4_CHAR:
      stateRelay4 = incomingMessage.value;
      
      outgoingMessage.property[0] = RELAY_4_CHAR;
      outgoingMessage.value       = stateRelay4;

      updateRelay4 = true;

      if (DEBUG){
        Serial.println();
        Serial.print("Set Relay 4 to: ");
        Serial.println(stateRelay4);
      }
      break;

      default:
      
      if (DEBUG){
        Serial.println();
        Serial.println("Command Not Recognized");
        pktError = true;
      }
      break;
    }
    outgoingMessage.action[0] = SET_CHAR;
    break;

    case EXECUTE_CHAR: //Execute
    switch (incomingMessage.property[0]) {

      case MOTION_CHAR: //Motion
      motorPositionSet = nm2stp(incomingMessage.value,PITCH_NM);
      updateMotion = true;

      outgoingMessage.property[0] = MOTION_CHAR;
      outgoingMessage.value    = motorPositionSet;

      if (DEBUG){
        Serial.println();
        Serial.print("Move to Position [nm]: ");
        Serial.println(motorPositionSet);
      }
      break;

      case CALIBRATION_CHAR: //Calibration
      outgoingMessage.property[0] = CALIBRATION_CHAR;
      outgoingMessage.value       = CONFIRM;
      executeCalibration           = true;

      if (DEBUG){
        Serial.println();
        Serial.println("Execute Calibration!");
      }
      break;

      case ABORT_CHAR: //abort
      outgoingMessage.property[0] = ABORT_CHAR;
      outgoingMessage.value       = CONFIRM;
      executeAbort                = true;

      if (DEBUG){
        Serial.println();
        Serial.println("Execute Abort");
      }
      break;

      case STEP_FWD_CHAR: //Step
      stepsToTake = nm2stp(incomingMessage.value,PITCH_NM);
      outgoingMessage.property[0] = STEP_FWD_CHAR;
      outgoingMessage.value       = CONFIRM;
      executeFwdStep              = true;

      if (DEBUG){
        Serial.print("Execute ");
        Serial.print(stepsToTake);
        Serial.println(" [nm] Forward Step");
      }
      break;

      case STEP_REV_CHAR: //Step
      stepsToTake = nm2stp(incomingMessage.value,PITCH_NM);
      outgoingMessage.property[0] = STEP_REV_CHAR;
      outgoingMessage.value       = CONFIRM;
      executeRevStep              = true;

      if (DEBUG){
        Serial.print("Execute ");
        Serial.print(stepsToTake);
        Serial.println(" [nm] Reverse Step");
      }
      break;


      default:

      if (DEBUG){
        Serial.println();
        Serial.println("Command Not Recognized");
      }
      pktError = true;
      break;
    }
    outgoingMessage.action[0] = EXECUTE_CHAR;
  }
}
