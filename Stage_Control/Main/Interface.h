#ifndef INTERFACE_H
#define INTERFACE_H


//Message Structure

//Template: <[action]:[property]:[value]>

//Markers
#define START_MARKER '<'
#define END_MARKER '>'
#define DELIM ":"

//Action
#define GET_CHAR 'G'
#define SET_CHAR 'S'
#define EXECUTE_CHAR 'E'
#define CHECK_CHAR 'C'

//Property

//->CHECK
#define IS_BUSY_CHAR 'B'
//#define CALIBRATION_CHAR 'C' (already defined)

//->SET
#define SHUTTER_POSITION_CHAR 'S'
#define RELAY_1_CHAR 'W'
#define RELAY_2_CHAR 'X'
#define RELAY_3_CHAR 'Y'
#define RELAY_4_CHAR 'Z'


//->GET
#define MOTOR_POSITION_CHAR 'P'
#define ENCODER_POSITION_CHAR 'N'
//#define SHUTTER_POSITION_CHAR 'S' (already defined)


//->EXECUTE
#define MOTION_CHAR 'M'
#define CALIBRATION_CHAR 'C'
#define ABORT_CHAR 'A'
#define STEP_FWD_CHAR  'F'
#define STEP_REV_CHAR  'R'


//Message Characteristics
//Formatting String: "<%c:%c:%.10lu>"

#define VALUE_LENGTH 10
#define HEADER_LENGTH 2
#define BUFFER_SIZE 15  //HEADER_LENGTH + #delim (=2) + VALUE_LENGTH + 1 (string terminator)
#define CONFIRM 1 //Value for message confirmation


struct Message{
    char            action[2];
    char            property[2];
    unsigned long   value = 0UL; // value is an unsigned long (32-bit)
};

//******************Functions*****************//

void receivePacket();

void sendPacket();

void parsePkt();

#endif
