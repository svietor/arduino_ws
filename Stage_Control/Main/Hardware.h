#ifndef HARDWARE_H
#define HARDWARE_H

#include <Arduino.h>
#include <SPI.h>
#include "SparkFunAutoDriver.h"
#include "LS7366R.h"
#include <Servo.h>

#include "Utilities.h"


//*********************Arduino***********************************//
//General Settings
#define BAUD_RATE 115200           // Arduino Baud rate
#define SPI_DELAY 100              // [ms] Delay before executing SPI function, to leave some breathing room between communicationss.
//***************************************************************//

//*********************AutoDriver********************************//
#define AUTODRIVER_CS 10 // SPI Slave Select Pin
#define RESET_PIN 7 // Reset pin
#define IS_BUSY_PIN 8 // See SPARKFUN AutoDriver Datasheet

#define STATUS_OK_BYTE 0x2E88 // See SPARKFUN AutoDriver Datasheet

//Motor Voltages (Vcc = 12V)
#define RUN_KVAL 128 // Running -> 64/255 * 12V = 3V
#define ACC_KVAL 128 // Acceleration -> 128/255 * 12V = 6V
#define DEC_KVAL 128 // Deceleration -> 128/255 * 12V = 6V
#define HOLD_KVAL 32 // Holding -> 32/255 * 12V = 1.5V

//Motor speed/accel settings
#define SPEED_VAL 300UL // Motor speed [uSteps/s]
#define ACCEL_VAL 400 // Motor acceleration [uSteps/s/s]
#define DECEL_VAL 400 // Motor deceleration [uSteps/s/S]

//#define CALIBRATION_STEP_SIZE 10 //Step size during calibration procedure [uStep]
#define CALIBRATION_STEP_SIZE 10
#define CALIBRATION_MARGIN 5 // allowable deviation from zero ticks for calibration procedure [ticks] 
//*****************************************************************//

//*********************Shutter Servo********************************//
#define SHUTTER_SERVO_PIN 3

//*****************************************************************//

//*********************Linear Stage********************************//
//Linear Stage Properties
#define STAGE_PITCH 10000000 //[nm/rot]
#define MICRO_STEPS 32 //Steps/step
#define STEPPER_COUNT 400 // [Step/rot] (Stepper motor ST5909L3008-B)
#define PITCH_NM 781UL // Stage pitch [nm/step]
#define STEP_COUNT_BITS 22 //number of bits of the stepcount register
//*****************************************************************//

//*********************Encoder & Buffer******************************//
#define ENCODER_CS 4 //SPI slave select for encoder buffer 
#define MDR0 X4_QUAD | FREE_RUN | CLK_DIV_1 //Configuration Register MDRO (see LS7366R datasheet)
#define MDR0_RST X4_QUAD | FREE_RUN | CLK_DIV_1 | RST_CNTR // enable counter reset on index pulse
#define MDR1 FOUR_BYTE  //Configuration Register MDR1 (see LS7366R datasheet)

//Encoder properties
#define RES_NM 10000 //Edge resolution 10[mu] -> 10000 NM/tick

//*****************************************************************//


//*****************************Relais******************************//
#define RELAY_1_PIN 1 //TODO: update!!
#define RELAY_2_PIN 2
#define RELAY_3_PIN 3
#define RELAY_4_PIN 4
//*****************************************************************//


void setupAutoDriver();

bool statusAutoDriver();

void stageCalibration();

#endif
